
### Exercise 7
**(With an intermezzo about file manipulation)**

You can play with the different parameters to see how the voltage
dynamics changes according to these parameters.

Here we would like you to systematically try different parameters and save the
produced plots as png files with names containing the parameter values (for
example `'test_v0.4_ri0.15_dt0.1_taum0.1.png'`).

To save the produced plots, you can use the function (in fact method)
`savefig(path)`:
```python
fig, ax = plt.subplots()
ax.plot(V)
fig.savefig("filepath.png")
```

Before being able to do so, you might need some information about how to
manipulate strings.
In the previous exercises you might have seen that it is possible to insert
values from variables within a string using the curved brackets `{` and `}`.

Simply put, the way it works is by putting the character `f` before your string
(they are called f-strings) and then everything within curved brackets will be
transformed into string if possible.
For example:
```python
V = [ 10.0 ]
f'test_v{V[0]}'
```
will produce the following string:
```python
'test_v10.0'
```
Note that if the `f` is not in front of the string, the curved brackets will be
interpreted as normal characters.

**For more on string manipulation, you can read
[there](https://docs.python.org/3/tutorial/inputoutput.html#input-and-output)**


```python
# Carry over here the previously declared variables and the code from the
previous exercises
```


```python
print(f'test_v{V[0]}_dt{dt}_taum{tau_m}')
print('test_v{V[0]}_dt{dt}_taum{tau_m}')
```

#### Avoiding cramping up you current folder
If you want to be a little bit cleaner, you can create a folder in which you
will save your images.

You can create such a folder directly in python using `Path` from the `pathlib`
library and the command:
```python
from pathlib import Path

Path.mkdir('<folder_name>')

```
For example, to create a folder named `question_7` one could run the command
```python
Path.mkdir('question_7')
```

Though, if the folder already exists, the command line will not work and stop
the notebook from running.
To avoid such a problem, it is possible to check whether the folder already
exists using the method `exists` of `Path` as shown below.

Let's create the folder `question_7`:

```python
from pathlib import Path
folder = Path('question_7')
if not folder.exists():
    Path.mkdir(folder)
```

#### Path manipulation
Some of you might already be aware that playing with paths can be a pain.
The problem comes from the fact that Windows has a different way to represent a
path to a folder than Linux and MacOs.

> **_Side Note: what's a path?!_**
>
> In a computer the folders and files are organised hierarchically.
> What it means is that each file or folder except for one, the root, is in a
> folder.
> For example, the folder you created earlier (`question_7`) is itself in a
> folder.
>
> To access a file or folder, it is sometimes necessary to know the sequence of
> folders it is in so there is no ambiguity for the computer.
> The sequence of folders a folder or a file belongs to is the **path** and it
> can be represented as a string.
> For example, you can call the function `Path.cwd` (for the current working
> directory).
> To query the list of directories your notebook is running in:


```python
print('Our current path:')
print(Path.cwd())
```

> You can maybe see that the folders are separated by a `/` (or a `\` for
> Windows).
> This difference between Linux or MacOs and Windows has been quite a source of
> trouble, some of you might have experienced it.

Now, to save an image in the folder `'question_7'`, as we would like to do, we
just need to concatenate the image name to the folder name:
```python
full_path = folder / 'test_a0.4_i0.15_dt0.001.png'
```

Note that the `/` in this case is a concatenation operator specific to the
objects of the `path` library. The operator concatenates two `Path` or a `Path`
and a `str` putting the operating specific folder separator (
`/` or `\`).

**More info about the `pathlib` can be found [there](https://docs.python.org/3/library/pathlib.html)**


```python
## folder is the path previously created
# Concatenation of two Paths
print(folder / Path('test_a0.4_i0.15_dt0.001.png'))
# Concatenation of a Path and a str (same result)
print(folder / 'test_a0.4_i0.15_dt0.001.png')
```

Now we can *cleanly* answer question 6.
Let assumes that we want the following values:
- `tau_m` changes from `10` to `20` and that we want `5` values within that
  interval
- `RI` changes from `0` to `100` and that we also want `5` values within that interval
- and a fixed `dt=0.1`

**Write some lines of code to compute and save the requested plots**

Note: you can use the function `np.linspace` to generate the desired values


```python
```

### Exercise 8
**This exercise is difficult, it might take a bit longer to solve.**

For this exercise, we will discuss about file manipulation, in other words how
to move files automatically.

**Attention here! Proceed with caution for this exercise but also in general.
Files removed using Python (or the shell for example) do not end up in the
trash but are directly removed!**

In this exercise we would like to sort the files created in the previous
exercise. We would like to group the plots generated previously in folders by
values of `tau_m`, so that the folders are named according to that `tau_m`
value.

For example if you had the following files in your `exercise_7` folder:
```
exercise_7:
 | taum10_ri00.png
 | taum10_ri10.png
 | taum10_ri20.png
 | taum20_ri00.png
 | taum20_ri10.png
 | taum20_ri20.png
 | taum30_ri00.png
 | taum30_ri10.png
 | taum30_ri20.png
```
We would like you to create the following hierarchy:
```
exercise_7:
 | taum10:
   | taum10_ri00.png
   | taum10_ri10.png
   | taum10_ri20.png
 | taum20:
   | taum20_ri00.png
   | taum20_ri10.png
   | taum20_ri20.png
 | taum30:
   | taum30_ri00.png
   | taum30_ri10.png
   | taum30_ri20.png
```

To do so you can use the following functions (assuming `p` is a `Path`):
- `Path.iterdir` allows to loop through all the files of a directory
- `p.name` retrieves the name of the file in `p` as a `str`
- `str.split` splits a string
- `Path.exists` see above
- `p.rename` allows to rename (and therefore move) `p`

Do not hesitate to look at the help of each of these functions (you should do
it!).


```python
p = Path('question_7')
for file in p.iterdir():
    ## Do things here
    print(file)
```

### Help for exercise 8
Because the difficulty increased significantly with this exercise, here are
some leads that hopefully will help you solve the exercise!

One way to solve a coding problem is to decompose it in multiple smaller
problems.
There are often multiple ways to decompose a problem, we will show you one
here, it might not be the optimal one (regardless of the optimal metric used)
but it should be a working one.
To build that decomposition, it can sometimes be useful to rephrase the problem
in terms of what you want the code to do:
