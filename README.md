# Introduction to python for neuroscientists

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/rouault-team-public%2Fteaching%2Fm1-neurocomp/HEAD)

## Requirements for the course.

### Recommended software

This course is made on a [jupyter notebook](https://jupyter.org/) running on
Python 3.9 or newer.

To install the enviroment on a local system, we recommend using a virtual
environment with:
- python 3.9 or above
- numpy
- scipy
- matplotlib

```
python3 -m venv venv
source venv/bin/activate
pip install pip --upgrade
pip install pip-tools
pip-sync
```

Otherwise, you can use a `mybinder` environment.

## Launching the notebook

Once you are in the virtual environment, launch `jupyter lab` and navigate to
`introduction-to-python`.

## License

This lectures are adapted from [Introduction to Python using Turing
patterns](https://github.com/GuignardLab/CenTuri-Course-2022)
by Léo Guignard licensed under the MIT license (see LICENSE file).

This work is now additionally licensed under [CC BY-NC-SA
4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)
